#!/bin/bash

# PASS-FILE by Claudio Barca (Copyright 2021 Claudio Barca)
# This software is distributed under GPL v. 3 license.
# See LICENSE file for details.

pass_command="$1"
pass_argument="$2"
pass_newentry="$3"

default_dir="$HOME/Downloads"


help(){
    echo "-- pass-file.sh v. 0.1 --"
    echo "save and show files in pass password store using base64"
    echo "  Usage:"
    echo "   pass-file.sh insert file-name entry-name"
    echo "      Insert a new file in the password store."
    echo "   pass-file.sh save entry-name"
    echo "      Decrypt and save the file in the current directory."
    echo "   pass-file.sh show entry-name"
    echo "      Decrypt and open the file in the xdg default app."
}


create_tmpfile(){
    pass_text=$(pass show $pass_argument)
    [ -z "$(echo "$pass_text" | grep "begin base64")" ] && exit
    pass_filename="$(echo "$pass_text" | sed -n '/filename:/p;d' | sed 's/filename: //')"
    pass_tmpfile_name="/tmp/pass_file-$pass_filename"
    echo "$pass_text" | sed -n '/^begin base64$/,/^end$/p' | sed '1d;$d' | base64 -d  > "$pass_tmpfile_name"
    # echo "$pass_text" | sed -n '/^begin base64$/,/^end$/p' | sed '1d;$d' | base64 -d | gunzip -c > "$pass_tmpfile_name"
}

show(){
    xdg-open "$pass_tmpfile_name" &
}

insert(){
    pass_path=$(ls "$pass_argument")
    [ -z "$pass_path" ] && exit
    pass_filename=$(echo $pass_path | awk -F '/' '{print $NF}')
    pass_base64=$(base64 "$pass_path")
    echo -e "\nfilename: $pass_filename\nbegin base64\n$pass_base64\nend" | pass insert -m $pass_newentry
}

save(){
    cp "$pass_tmpfile_name" "$default_dir/$pass_filename"
    echo "File saved in $default_dir/$pass_filename"
    notify-send  "File saved in $default_dir/$pass_filename"
    }

mkdir -p $default_dir
case $pass_command in
    help)
	help
	;;
    insert)
	insert
	;;
    save)
	create_tmpfile
	save
	;;
    show)
	create_tmpfile
	show
	;;
    *)
	help
	;;
esac
