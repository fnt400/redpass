# REDPASS

Redpass is a simple bash script that uses dmenu to create a gui for the "GNU Pass" password manager. 

With that you can generate new passwords, automatically fill the forms in your browser and in other programs. You can also insert various fields (called attributes) associated to you main password and generate OTP.

Furthermore, you can bundle small files in the entry (for example the photo of your ID) and easily recall it later.

Everything will be securely encrypted with GPG and can be synced between devices with GIT.

## INSTALL

In order to use Redpass you must have installed and working properly the following programs (to install them you can follow the instructions present in their websites):

- [GNU Pass](https://www.passwordstore.org/)
- [dmenu](http://tools.suckless.org/dmenu/)

Optionally, you could need:

- To handle One Time Password (OTP):
[pass-otp](https://github.com/tadfisher/pass-otp)

- To create Passphrases:
[xkcdpass](https://pypi.org/project/xkcdpass/)

To install Redpass you can clone the git repository from GitLab:

```
$ git clone https://gitlab.com/fnt400/redpass.git
```

Than launch the install.sh script. It will copy the scripts in your local bin folder (~/.local/bin/). In order to work properly it has to be included in your path.

## USAGE

Redpass project consists in 3 scripts:

- redpass: the main dmenu gui
- pass-file: a command-line tool that encrypts files in the Pass entry and decrypts and opens it later
- pass-field: a command-line tool that displays a single field of an entry (for the example a phone number or an email)

### CREATING ENTRIES

Simply start the "redpass" script. A red dmenu will appear at the bottom of the screen, containing all the existing entries in you GNU Pass password store.

You can create a new entry selecting the first choice "New...".

Than insert the title of the entry (that will appear in the main menu). To create or use a separated folder use "/" in the title (for example: mail/gmail).

Than you have to insert the "user" field. It is mandatory and will be created as a particular line of text like:
user: my_user_name

Than you have to create the password. You can insert it manually of let the computer create it randomly. You have three choice:

1. 20 characters without symbols
2. 16 characters with symbols
3. 8 English random words (you'll need xkcdpass installed)

Than you can add optional fields containing other data using this format:

field_title: my_field_data

Or you can add and OTP data pasting the TOTP Secret (you'll need otp-pass installed).


Hit return to save the entry and exit.
You have done!

### RECALLING ENTRIES

Simply start the program and select the right entry. Pressing return the next menu will display user, password and all the other field you have saved.
Selecting one of those, they will be printed in the standard output. 

The "User and Password" choice will print first the user, than a tabulation and than the password. It is useful for compiling the web forms with just a few clicks.

Selecting the OTP voice a proper one time password will be generated and printed to the standard output.

### MODIFYING ENTRIES

You can rename, delete or edit and entry selecting the proper voice of the menu.

### HANDLING FILES

Inserting a file in an entry can be done with the command-line tool pass-file. Here's the syntax:

```
$ pass-file insert ./my_path/my_file.pdf my_entry
```

Than you can recall it with the gui or with the command line itself:

```
$ pass-file show my_entry
```

will show the file with the default application.

```
$ pass-file save my_entry
```

will save the file in the "~/Downloads/" directory. 

You can save just one file for entry.

### PASS-FIELD

To recall only a particular field for an entry (useful for scripting) you can use the command-line tool pass-field, with this syntax:

```
$ pass-field my-entry chosen_field
```

## SOURCE CODE

You can find the source code on GitLab:

[Redpass source code](https://gitlab.com/fnt400/redpass)

## License

This software is distributed under GPL v. 3 license.
See LICENSE file for details.

